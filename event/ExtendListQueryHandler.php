<?php

namespace Empu\FineControl\Event;

use Backend\Facades\BackendAuth;

/**
 * ExtendListQueryHandler
 */
class ExtendListQueryHandler
{
    /**
     * summary
     */
    public function handle ($widget, $query)
    {
        $user = BackendAuth::getUser();

        if (! $user->isSuperUser()) {
            $this->extendUserQuery($widget, $query);
            $this->extendUserGroupQuery($widget, $query);
        }
    }

    protected function extendUserQuery($widget, $query) {
        if (
            $widget->getController() instanceof \Backend\Controllers\Users &&
            $widget->model instanceof \Backend\Models\User
        ) {
            $query->whereHas('groups', function ($q) {
                $q->where('backend_user_groups.id', '>', 1);
            });
        }
    }

    protected function extendUserGroupQuery($widget, $query)
    {
        if (
            $widget->getController() instanceof \Backend\Controllers\UserGroups &&
            $widget->model instanceof \Backend\Models\UserGroup
        ) {
            $query->where('id', '>', 1);
        }
    }

}
