<?php

namespace Empu\FineControl\Event;

use Backend\Facades\BackendAuth;
use Backend\Models\UserGroup;

/**
 * ExtendFormFieldsHandler
 */
class ExtendFormFieldsHandler
{
    /**
     * Handler
     */
    public function handle($widget, $fields)
    {
        // Only for the User controller & the User model
        if (
            $widget->getController() instanceof \Backend\Controllers\Users
            && $widget->model instanceof \Backend\Models\User
        ) {
            $this->extendUserFields($widget, $fields);
        }
    }

    protected function extendUserFields($widget, $fields)
    {
        $user = BackendAuth::getUser();

        // jika bukan superuser, tampilkan pilihan group tanpa "group of masters"
        if (! $user->isSuperUser()) {
            $widget->addTabFields([
                'groups' => [
                    'context' => ['create', 'update'],
                    'label' => 'backend::lang.user.groups',
                    'commentAbove' => 'backend::lang.user.groups_comment',
                    'type' => 'checkboxlist',
                    'options' => $this->getGroupsOptions()
                ]
            ]);
        }

        // jika tidak memiliki hak, hapus ubah suai izin user
        if (! $user->hasAccess('backend.manage_user_permissions')) {
            $widget->removeTab('backend::lang.user.permissions');
        }
    }

    protected function getGroupsOptions()
    {
        $result = [];

        foreach (UserGroup::where('id', '<>', 1)->get() as $group) {
            $result[$group->id] = [$group->name, $group->description];
        }

        return $result;
    }
}
